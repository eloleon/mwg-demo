import React, { Component } from 'react';
import Routes from './routes';

import { BrowserRouter as Router } from 'react-router-dom'

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <div className="App">
            <div className="App-header">
              <img src={process.env.PUBLIC_URL + '/assets/mwgLogoWhite.png'} className="App-logo" alt="MyWebGrocer logo" />
            </div>
            {this.props.children}
          </div>
          {<Routes />}
        </div>
      </Router>
    );
  }
}

export default App;
