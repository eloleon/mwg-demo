import React, { Component } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

export default class Page extends Component {

  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.array
    ]),
    className: PropTypes.string
  }

  render () {
    const { className } = this.props;

    return (
      <div className={classnames("page-container", [className])}>{this.props.children}</div>
    );
  }
}
