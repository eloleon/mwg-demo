import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const MyLink = ({ caption, className, url }) => {
  const to = { pathname: url };
  return (
    <Link className={className} to={to}>
      {caption}
    </Link>
  );
};

MyLink.propTypes = {
  caption: PropTypes.string,
  className: PropTypes.string,
  url: PropTypes.string
};

export default MyLink;
