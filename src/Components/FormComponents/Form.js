import React from 'react';
import { Field, reduxForm } from 'redux-form';
import FormField from './FormField';
import { dob, email, required } from '../../Utils/FormUtils/formValidators';
import PropTypes from 'prop-types';

const MyWebGrocerForm = props => {
  const { handleSubmit, submitting, valid } = props;
  return (
    <div className="mwg-column-container">
      <form onSubmit={handleSubmit}>
        <div className="mwg-row">
          <div className="mwg-column mgw-50">
            <Field
              name="firstName"
              type="text"
              component={FormField}
              label="First Name"
              placeholder="John"
              validate={[required]}
            />
          </div>
          <div className="mwg-column mgw-50">
            <Field
              name="surname"
              type="text"
              component={FormField}
              label="Surname"
              placeholder="Doe"
              validate={[required]}
            />
          </div>
        </div>
        <div className="mwg-row">
          <div className="mwg-column mgw-50">
            <Field
              name="dob"
              type="text"
              component={FormField}
              label="Date of Birth"
              placeholder="01/01/1980"
              validate={[required, dob]}
            />
          </div>
          <div className="mwg-column mgw-50">
            <Field
              name="nationality"
              type="text"
              component={FormField}
              label="Nationality"
              placeholder="Irish"
              validate={[required]}
            />
          </div>
        </div>
        <div className="mwg-row">
          <div className="mwg-column mgw-50">
            <Field
              name="email"
              type="email"
              component={FormField}
              label="Email"
              placeholder="hello@info.com"
              validate={[required, email]}
            />
          </div>
          <div className="mwg-column mgw-50">
            <Field
              name="occupation"
              type="text"
              component={FormField}
              label="Occupation"
              placeholder="Front End Developer"
              validate={[required]}
            />
          </div>
        </div>
        <div>
          <button
            type="submit"
            disabled={submitting || !valid}
            className="btn btn-primary float-right"
          >
            Submit
          </button>
        </div>
      </form>
    </div>
  );
};

MyWebGrocerForm.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool,
  valid: PropTypes.bool
};

export default reduxForm({
  form: 'mwg-demo'
})(MyWebGrocerForm);
