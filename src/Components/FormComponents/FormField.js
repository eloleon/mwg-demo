import React from 'react';
import PropTypes from 'prop-types';

const formField = ({ input, label, type, placeholder, meta: { touched, error, valid } = {} }) => {
  const inputClass = touched ? (valid ? 'valid' : error ? 'error' : '') : '';
  return (
    <div>
      <label>{label}</label>
      <div className="relative">
        <input {...input} placeholder={placeholder} type={type} className={inputClass} />
        {touched && error && <span className="mwg-input-error">{`${label} ${error}`}</span>}
        {touched && valid && <span className="mwg-input-valid" />}
      </div>
    </div>
  );
};

formField.propTypes = {
  input: PropTypes.object,
  label: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string,
    valid: PropTypes.bool
  })
};

export default formField;
