import React from 'react';
import Page from '../Components/Primitives/Page';

const ThankYouPage = () => {
  return (
    <Page className="page-thank-you">
      <h2>Thank You</h2>
    </Page>
  );
}

export default ThankYouPage;
