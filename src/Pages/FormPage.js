import React, { Component } from 'react';
import Page from '../Components/Primitives/Page';
import Form from '../Components/FormComponents/Form';
import PropTypes from 'prop-types';

export default class FormPage extends Component {

  static propTypes = {
    history: PropTypes.shape({
      push: PropTypes.func
    })
  }

  /**
  * This is a function that will be called when form is submitted.
  * It will redirect users to Thank You page.
  * In real-life you would dispatch action on submit
  */
  handleSubmit = () => {
    this.props.history.push('/thank-you')
  }

  render(){
    return (
      <Page className="page-form">
        <Form handleSubmit={this.handleSubmit} />
      </Page>
    )
  }
}
