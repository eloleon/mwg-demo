import React from 'react';
import Page from '../Components/Primitives/Page';
import Link from '../Components/Primitives/Link';

const WelcomePage = () => {
  return (
    <Page className="page-welcome">
      <h2>Welcome</h2>
      <p>
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
        been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
        galley of type and scrambled it to make a type specimen book.
      </p>
      <p>
        It has survived not only five centuries, but also the leap into electronic typesetting,
        remaining essentially unchanged. It was popularised in the 1960s with the release of
        Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing
        like Aldus PageMaker including versions of Lorem Ipsum.
      </p>
      <Link className="btn float-right" url="form" caption="Next" />
    </Page>
  );
};

export default WelcomePage;
