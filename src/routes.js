import React from 'react';
import { Route, Switch } from 'react-router-dom';

import WelcomePage from './Pages/WelcomePage';
import ThankYouPage from './Pages/ThankYouPage';
import FormPage from './Pages/FormPage';

const Routes = () => (
  <div>
    <Switch>
      <Route exact path="/" component={WelcomePage} />
      <Route path="/form" component={FormPage} />
      <Route path="/thank-you" component={ThankYouPage} />
    </Switch>
  </div>
);

export default Routes;
