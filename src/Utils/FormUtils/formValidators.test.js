import { expect } from 'chai';
import { dob, email, required } from './formValidators';

describe('dob', () => {
  it("should return 'Invalid date format, must be dd/mm/yyyy' when format is not correct", () => {
    // Given
    const input = '01-01-2017';
    // When
    const output = dob(input);
    // Then
    expect(output).to.equal('Invalid date format, must be dd/mm/yyyy');
  });

  it('should return undefined when format is correct', () => {
    // Given
    const input = '01/01/2017';
    // When
    const output = dob(input);
    // Then
    expect(output).to.be.undefined;
  });
});

describe('email', () => {
  it("should return 'Invalid email address' when format is not correct", () => {
    // Given
    const input = 'WrongEmailAddress@';
    // When
    const output = email(input);
    // Then
    expect(output).to.equal('Invalid email address');
  });

  it('should return undefined when format is correct', () => {
    // Given
    const input = 'info@mwg.ie';
    // When
    const output = email(input);
    // Then
    expect(output).to.be.undefined;
  });
});

describe('required', () => {
  it("should return 'is required' when value is undefined", () => {
    // Given
    const input = '';
    // When
    const output = required(input);
    // Then
    expect(output).to.equal('is required');
  });

  it('should return undefined when format is correct', () => {
    // Given
    const input = 'Some value';
    // When
    const output = required(input);
    // Then
    expect(output).to.be.undefined;
  });
});
