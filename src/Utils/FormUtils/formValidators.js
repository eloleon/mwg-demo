export const required = value => value ? undefined : 'is required';
export const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Invalid email address'
    : undefined;
export const dob = value =>
  value && !/^[0-9]{2}[/]{1}[0-9]{2}[/]{1}[0-9]{4}$/g.test(value)
    ? 'Invalid date format, must be dd/mm/yyyy'
    : undefined;
